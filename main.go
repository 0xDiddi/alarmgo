package main

import (
	"flag"
	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
	"go.uber.org/zap"
	"os"
	"time"
)

func main() {
	logger, err := zap.Config{
		Level:            zap.NewAtomicLevelAt(zap.DebugLevel),
		Development:      true,
		Encoding:         "console",
		EncoderConfig:    zap.NewDevelopmentEncoderConfig(),
		OutputPaths:      []string{"stdout", "./alarm.log"},
		ErrorOutputPaths: []string{"stderr", "./alarm.log"},
	}.Build()

	if err != nil {
		panic(err)
	}

	timeArg := flag.String("time", "", "The time when to trigger.")
	sleepCycleTimeArg := flag.String("ct", "10m", "The delay between wakes.")
	flag.Parse()

	timeEnd, err := time.Parse(time.RFC3339, *timeArg)
	if err != nil {
		logger.Fatal("couldn't parse time", zap.Error(err))
	}

	remainingSleep := time.Until(timeEnd)
	if remainingSleep < time.Second*10 {
		logger.Fatal("target time must be at least 10 seconds in the future",
			zap.Time("target", timeEnd), zap.Duration("delta", remainingSleep))
	}

	sleepCycleTime, err := time.ParseDuration(*sleepCycleTimeArg)
	if err != nil {
		logger.Fatal("couldn't parse delay", zap.Error(err))
	}

	if sleepCycleTime < time.Second*10 {
		logger.Fatal("cycle time must be at least 10 seconds",
			zap.Duration("cycle time", sleepCycleTime))
	}

	if remainingSleep > sleepCycleTime {
		logger.Info("delta over threshold, entering deep sleep…", zap.Duration("delta", remainingSleep))

		for repeat := true; repeat; {
			remainingSleep = time.Until(timeEnd) - sleepCycleTime
			currentSleepInterval := time.Duration(0)
			if remainingSleep > sleepCycleTime {
				currentSleepInterval = sleepCycleTime
			} else {
				currentSleepInterval = remainingSleep
				repeat = false
			}

			logger.Info("going for another round of deep sleep", zap.Duration("remaining", remainingSleep), zap.Duration("sleep", currentSleepInterval))
			time.Sleep(currentSleepInterval)
		}
	} else {
		logger.Info("delta under threshold, skipping deep sleep…", zap.Duration("delta", remainingSleep))
	}

	logger.Info("loading file")

	f, err := os.Open("SIREN.mp3")
	if err != nil {
		logger.Fatal("couldn't open file", zap.Error(err))
	}
	//noinspection GoUnhandledErrorResult
	defer f.Close()

	s, format, err := mp3.Decode(f)
	if err != nil {
		logger.Fatal("couldn't decode mp3", zap.Error(err))
	}

	err = speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/20))
	if err != nil {
		logger.Fatal("couldn't init speaker", zap.Error(err))
	}

	done := make(chan bool)

	logger.Info("constructing buffer")

	buf := beep.NewBuffer(format)
	buf.Append(beep.Take(format.SampleRate.N(time.Second*22), s))
	intro := buf.Streamer(0, format.SampleRate.N(time.Second*10))
	loop := beep.Loop(-1, buf.Streamer(format.SampleRate.N(time.Second*10), format.SampleRate.N(time.Second*20)))

	remainingSleep = time.Until(timeEnd)
	logger.Info("streams constructed, final countdown commencing", zap.Duration("delta", remainingSleep))

	time.Sleep(remainingSleep)

	logger.Info("commencing playback", zap.Time("current", time.Now()))

	speaker.Play(beep.Seq(intro, loop, beep.Callback(func() { close(done) })))

	<-done
}
